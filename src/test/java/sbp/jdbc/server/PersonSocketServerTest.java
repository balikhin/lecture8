package sbp.jdbc.server;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import sbp.jdbc.dao.WorkWithTable;
import sbp.jdbc.model.Person;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Тестирование взаимодействия с БД с использованием {@link PersonSocketServer}
 */
public class PersonSocketServerTest
{

    private final PersonSocketServer server = new PersonSocketServer(8080, new WorkWithTable());
    private final WorkWithTable personRepo = new WorkWithTable();
    private final List<Person> list = new ArrayList<>();

    @Before
    public void start()
    {
        list.add(new Person("Vasa", "Moscow", 22));
        list.add(new Person("Masha", "Omsk", 19));
        list.add(new Person("Oleg", "Sochy", 27));

        personRepo.createPerson(list.get(0));
        personRepo.createPerson(list.get(1));
        personRepo.createPerson(list.get(2));
    }

    @Test
    public void serverTest()
    {
        try
        {
            server.start();
        } catch (IOException e)
        {
            e.printStackTrace();
        }
    }

    @After
    public void end()
    {
        personRepo.deleteTable();
    }
}

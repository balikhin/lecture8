package sbp.jdbc.server;

import com.sun.net.httpserver.HttpServer;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import sbp.jdbc.dao.WorkWithTable;
import sbp.jdbc.model.Person;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.util.ArrayList;
import java.util.List;

import static com.sun.net.httpserver.HttpServer.create;

/**
 * Тестирование взаимодействия с БД с использованием {@link PersonHTTPHandler}
 */
public class PersonHTTPServerTest
{
    private HttpServer server;
    private final WorkWithTable personRepo = new WorkWithTable();
    private final List<Person> list = new ArrayList<>();

    @Before
    public void start()
    {
        list.add(new Person("Vasa", "Moscow", 22));
        list.add(new Person("Masha", "Omsk", 19));
        list.add(new Person("Oleg", "Sochy", 27));

        personRepo.createPerson(list.get(0));
        personRepo.createPerson(list.get(1));
        personRepo.createPerson(list.get(2));


    }

    @Test
    public void serverTest()
    {
        try
        {
            server = create(new InetSocketAddress("localhost", 8080), 0);
            server.createContext("/", new PersonHTTPHandler(personRepo));
            server.start();
            System.out.println("Server started.");

        } catch (IOException e)
        {
            e.printStackTrace();
        }

    }

    @After
    public void end()
    {
      personRepo.deleteTable();
    }
}

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
    <head>
        <style>
            <%@ include file="css/style.css"%>
        </style>
        <link rel="stylesheet" type="text/css" href="${request.contextPath}/css/style.css" />
        <title>О сайте</title>
    </head>

    <body>
        <hr>
        <h3>Приложение 1н</h3>
        <div>
            Курс от Сбербанк Университета. Задание 8.
        </div>
        <td><a href="index.jsp">На главную</a></td>
    </body>
</html>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<html>
    <head>
        <style>
            <%@ include file="css/style.css"%>
        </style>
        <link rel="stylesheet" type="text/css" href="${request.contextPath}/css/style.css" />
        <title></title>
    </head>

    <body>
        <h1>Сбербанк Университет основы программирования</h1>
        <br>
        <h3>ЗАДАЧИ ПРОЕКТА:</h3>
        <div>
            Изучение Java достаточно сложное занятие, но если продолжать и не бросать, все будет получаться
        </div>
        <hr>
        <div id="tabl">
            <form action="Edit" method="post">
                <div aling="center">
                    <table border="1" cellpadding="5">
                        <caption><h2>Список участников:</h2></caption>
                        <tr>
                            <th>ID</th>
                            <th>Имя</th>
                            <th>Город</th>
                            <th>Возраст</th>
                            <th>Редактировать</th>
                            <th>Удалить</th>
                        </tr>
                        <c:forEach var="person" items="${personsList}">
                            <tr>
                                <td><c:out value="${person.id}" /></td>
                                <td><c:out value="${person.personName}" /></td>
                                <td><c:out value="${person.city}" /></td>
                                <td><c:out value="${person.age}" /></td>
                                <td><a href="${pageContext.servletContext.contextPath}/edit?id=${person.id}">Редактировать</a></td>
                                <td><a href="${pageContext.servletContext.contextPath}/delete?id=${person.id}">Удалить</a></td>
                            </tr>
                        </c:forEach>
                    </table>
                </div>
                <br>
                <td><a href="${pageContext.servletContext.contextPath}/add">Добавить пользователя</a></td>
            </form>
        </div>
        <hr>
        <td><a href="about.jsp">О сайте</a></td>
    </body>
</html>
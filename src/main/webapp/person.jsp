<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html xmlns="http://www.w3.org/1999/xhtml">
   <head>
       <style>
           <%@ include file="css/style.css"%>
       </style>
       <link rel="stylesheet" type="text/css" href="${request.contextPath}/css/style.css" />
       <title>Добавить/Изменить пользователя</title>
   </head>

   <body>
      <div align="center">
         <c:if test="${person != null}">
            <form action="${pageContext.servletContext.contextPath}/edit" method="POST">
         </c:if>
         <c:if test="${person == null}">
            <form action="${pageContext.servletContext.contextPath}/add" method="POST">
         </c:if>

               <table border="1" cellpadding="4">
               <caption>
                  <h2>
                     <c:if test="${person != null}">
                        Редактировать пользователя
                     </c:if>
                     <c:if test="${person == null}">
                        Добавить пользователя
                     </c:if>
                  </h2>
               </caption>

               <c:if test="${person != null}">
                  <input type="hidden" name="id" value="<c:out value='${person.id}' />" />
               </c:if>
               <tr>
                  <th>Имя пользователя: </th>
                  <td>
                     <input type="text" name="personName" size="45" value="<c:out value='${person.personName}' />" />
                  </td>
               </tr>
               <tr>
                  <th>Город: </th>
                  <td>
                     <input type="text" name="city" size="45" value="<c:out value='${person.city}' />" />
                  </td>
               </tr>
               <tr>
                  <th>Возраст: </th>
                  <td>
                     <input type="text" name="age" size="3" value="<c:out value='${person.age}' />" />
                  </td>
               </tr>
               <tr>
                  <td colspan="2" align="center">
                     <input type="submit" value="Сохранить" />
                  </td>
               </tr>
               </table>
            </form>
      </div>
   </body>
</html>

package sbp.jdbc.server;

import com.google.gson.Gson;
import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;
import sbp.jdbc.dao.WorkWithTable;
import sbp.jdbc.model.Person;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.nio.charset.StandardCharsets;
import java.util.stream.Collectors;


public class PersonHTTPHandler implements HttpHandler
{

    private final WorkWithTable personRepo;

    public PersonHTTPHandler(WorkWithTable personRepo)
    {
        this.personRepo = personRepo;
    }

    /**
     * Взаимодействие с БД в соответствии с поступившим запросом:
     * - GET по адресу /users возвращает список пользователей из БД
     * - GET по адресу /user/? возвращает пользователя с id=? из БД
     * - POST по адресу /user записывает поступившего пользователя в БД
     * - PUT по адресу /user редактирует поступившего пользователя в БД
     * - DELETE по адресу /user/? удаляет из БД пользователя с id=?
     * @param exchange
     * @throws IOException
     */
    @Override
    public void handle(HttpExchange exchange)
    {
        StringBuilder htmlBuilder = new StringBuilder();

        String[] uriR = exchange.getRequestURI().toString().split("/");
        Gson gson = new Gson();

        try (OutputStream outputStream = exchange.getResponseBody())
        {

            if (exchange.getRequestMethod().equalsIgnoreCase("GET"))
            {
                htmlBuilder = getRequest(uriR);
            }

            if (exchange.getRequestMethod().equalsIgnoreCase("POST"))
            {
                htmlBuilder = postRequest(uriR, exchange, gson);
            }

            if (exchange.getRequestMethod().equalsIgnoreCase("PUT")) {
                htmlBuilder = putRequest(uriR, exchange, gson);
            }

            if (exchange.getRequestMethod().equalsIgnoreCase("DELETE"))
            {
                htmlBuilder = deleteRequest(uriR);
            }

            String htmlStr = htmlBuilder.toString();
            exchange.sendResponseHeaders(200, htmlStr.length());

            outputStream.write(htmlStr.getBytes(StandardCharsets.UTF_8));
            outputStream.flush();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Формирования Html-страницы
     * @param str
     * @return
     */
    private StringBuilder getHtmlString(String str)
    {
        StringBuilder result = new StringBuilder();
        result.append("<html>")
                .append("<body>")
                .append("<h1>")
                .append(str)
                .append("</h1>")
                .append("</body")
                .append("</html>");
        System.out.println(result);
        return result;
    }

    private StringBuilder getRequest(String[] uriR) throws Exception
    {
        String str = "";

        if (uriR[1].equalsIgnoreCase("users"))
        {
            str = personRepo.findAllPerson().toString();

        }

        if (uriR[1].equalsIgnoreCase("user"))
        {
            str = personRepo.findPersonById(Integer.parseInt(uriR[2])).toString();
        }

        return getHtmlString(str);
    }

    private StringBuilder postRequest(String[] uriR, HttpExchange exchange, Gson gson)
    {
        if (uriR[1].equalsIgnoreCase("user"))
        {
            String jsonPerson = new BufferedReader(new InputStreamReader(exchange.getRequestBody()))
                    .lines()
                    .collect(Collectors.joining());

            Person person = gson.fromJson(jsonPerson, Person.class);
            personRepo.createPerson(person);
            return getHtmlString("Add");
        }

        return null;
    }

    private StringBuilder putRequest(String[] uriR, HttpExchange exchange, Gson gson)
    {
        if (uriR[1].equalsIgnoreCase("user"))
        {
            String jsonPerson = new BufferedReader(new InputStreamReader(exchange.getRequestBody()))
                    .lines()
                    .collect(Collectors.joining());

            Person person = gson.fromJson(jsonPerson, Person.class);
            personRepo.updatePerson(person);
            return getHtmlString("Edit");
        }

        return null;
    }

    private StringBuilder deleteRequest(String[] uriR) throws Exception
    {
        if (uriR[1].equalsIgnoreCase("user"))
        {
            personRepo.findPersonById(Integer.parseInt(uriR[2]));
            return getHtmlString("Deleted");
        }

        return null;
    }
}

package sbp.jdbc.server;

import com.google.gson.Gson;
import sbp.jdbc.dao.WorkWithTable;
import sbp.jdbc.model.Person;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.List;


public class PersonSocketServer
{

    private final int port;
    private final WorkWithTable personRepo;
    private final Gson gson;

    public PersonSocketServer(int port, WorkWithTable personRepo)
    {
        this.port = port;
        this.personRepo = personRepo;
        this.gson = new Gson();
    }

    /**
     * запуск Socket сервера
     * @throws IOException
     */
    public void start() throws IOException
    {
        ServerSocket serverSocket = new ServerSocket(port);

        while (true)
        {
            Socket socket = serverSocket.accept();

            try
            {
                workWithPersonDB(socket);
            } catch (Exception e)
            {
                e.printStackTrace();
            } finally {
                socket.close();
            }
        }
    }

    /**
     * Взаимодействие с БД в соответствии с поступившим запросом:
     * - GET по адресу /users возвращает список пользователей из БД
     * - GET по адресу /user/? возвращает пользователя с id=? из БД
     * - POST по адресу /user записывает поступившего пользователя в БД
     * - PUT по адресу /user редактирует поступившего пользователя в БД
     * - DELETE по адресу /user/? удаляет из БД пользователя с id=?
     * @param socket
     * @throws IOException
     */
    private void workWithPersonDB(Socket socket) throws Exception
    {
        char[] message = new char[512];
        try (BufferedReader reader = new BufferedReader(new InputStreamReader(socket.getInputStream())))
        {
            reader.read(message);
        } catch (IOException e)
        {
            e.printStackTrace();
        }
        String in = String.valueOf(message);

        if (in.contains("GET") & in.contains("/users"))
        {
            try (PrintWriter writer = new PrintWriter(socket.getOutputStream()))
            {
                List<Person> list = personRepo.findAllPerson();

                writer.write("HTTP/1.1 200 ОК\n");
                writer.println();
                if (list.size() != 0) writer.write(list.toString());
                writer.flush();
            } catch (IOException e) {
                e.printStackTrace();
            } catch (Exception e)
            {
                e.printStackTrace();
            }
        }

        if (in.contains("GET") & in.contains("/user"))
        {
            try (PrintWriter writer = new PrintWriter(socket.getOutputStream());)
            {
                int end = in.indexOf("HTTP") - 1;
                int id = Integer.parseInt(in.substring(10, end));
                Person person = personRepo.findPersonById(id);

                writer.write("HTTP/1.1 200 ОК\n");
                writer.println();
                if (person != null) writer.write(person.toString());
                writer.flush();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        if (in.contains("POST") & in.contains("user"))
        {
            Person person = gson.fromJson(getBody(in), Person.class);

            personRepo.createPerson(person);
        }

        if (in.contains("PUT") & in.contains("user"))
        {
            Person person = gson.fromJson(getBody(in), Person.class);

            personRepo.updatePerson(person);
        }

        if (in.contains("DELETE") & in.contains("user"))
        {
            int end = in.indexOf("HTTP") - 1;
            int id = Integer.parseInt(in.substring(10, end));

            personRepo.deletePerson(id);
        }
    }

    /**
     * Возвращает тело запроса
     * @param str
     * @return
     */
    private String getBody(String str)
    {
        return str.substring(str.indexOf("{"),str.lastIndexOf("}") + 1);
    }
}

package sbp.jdbc.service;

import sbp.jdbc.model.Person;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

/**
 * Класс взаимодействия с базой данных
 */
public class PersonService
{

    private Connection connection;
    private PreparedStatement preparedStatement;
    private Properties property = new Properties();
    private Object Optional;


    /**
     * База данных с которой осуществляется взаимодействие
     */
    private String jdbcUrl()
    {
        return "jdbc:sqlite:C:\\1\\TestDB.db";
    }

    /**
     * Установка соединения с базой данных
     *
     * @return соединение если удачно, иначе null
     */
    public PreparedStatement buildPreparedStatement(String query)
    {
        try
        {
            Class.forName("org.sqlite.JDBC");
            this.connection = DriverManager.getConnection(jdbcUrl());
            return preparedStatement = connection.prepareStatement(query);
        } catch (SQLException | ClassNotFoundException e)
        {
            e.printStackTrace();
            return null;
        }
    }


    /**
     * Закрытие соединения с базой данных
     */
    public void baseConnectionClose()
    {
        try
        {
            this.preparedStatement.close();
            this.connection.close();

        } catch (SQLException e)
        {
            e.printStackTrace();
        }
    }

    /**
     * Поиск записей в базе данных
     *
     * @param query - SQL запрос SELECT
     * @return список найденных согласно запросу
     */
    public List<Person> executeQuery(String query) throws Exception
    {
        List<Person> list = new ArrayList<>();
        try (PreparedStatement preparedStatement = buildPreparedStatement(query); ResultSet resultSet = preparedStatement.executeQuery(); CloseResources closeResources = new CloseResources())
        {
            while (resultSet.next())
            {
                Person person = new Person(resultSet.getInt(1), resultSet.getString(2), resultSet.getString(3), resultSet.getInt(4));

                list.add(person);
            }

            return list;
        } catch (SQLException e)
        {
            e.printStackTrace();
        } finally
        {
            baseConnectionClose();
        }
        return null;
    }

    /**
     * Потокобезопасное внесение изменений в базу данных
     *
     * @param query - SQL запрос на INSERT, UPDATE, DELETE
     * @return 0 если нет изменений, иначе количество измененных записей
     */
    public synchronized int executeUpdate(String query)
    {

        try (PreparedStatement preparedStatement = buildPreparedStatement(query))
        {
            int result = preparedStatement.executeUpdate();
            return result;
        } catch (SQLException e)
        {
            e.printStackTrace();
            return 0;
        }
    }
}
package sbp.jdbc.servlets;

import sbp.jdbc.dao.WorkWithTable;

import sbp.jdbc.model.Person;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

/**
 * Сервлет обработки стартовой страницы
 */
@WebServlet("/")
public class StartServlet extends HttpServlet
{

    WorkWithTable personRepo = new WorkWithTable();

    /**
     * Обработка GET запроса.
     * Вывод всех пользователей из базы данных.
     * @param req
     * @param resp
     * @throws ServletException
     * @throws IOException
     */
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException
    {
        resp.setContentType("text/html");
        List<Person> list = null;
        try
        {
            list = personRepo.findAllPerson();
        } catch (Exception e)
        {
            e.printStackTrace();
        }
        req.setAttribute("personsList", list);
        getServletContext().getRequestDispatcher("/index.jsp").forward(req, resp);
    }
}
package sbp.jdbc.servlets;

import sbp.jdbc.dao.WorkWithTable;


import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Сервлет удаления пользователя
 */
@WebServlet("/delete")
public class DeleteServlet extends HttpServlet
{

    WorkWithTable  personRepo = new WorkWithTable();

    /**
     * Обработка GET запроса.
     * Удаляет пользователя из базы данных по ID,
     * с использованием {@link sbp.jdbc.dao.WorkWithTable}.
     * После удаления возвращает на стартовую страницу.
     * @param req
     * @param resp
     */
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp)
    {
        int id = Integer.parseInt(req.getParameter("id"));
        personRepo.deletePerson(id);
        try {
            resp.sendRedirect("http://localhost:8080/Lecture6_war");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}

package sbp.jdbc.servlets;

import sbp.jdbc.dao.WorkWithTable;

import sbp.jdbc.model.Person;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Сервлет редактирования пользователя
 */
@WebServlet("/edit")
public class EditServlet extends HttpServlet
{

    WorkWithTable personRepo = new WorkWithTable();

    /**
     * Обработка GET запроса.
     * Поиск пользователя по ID и передача его на страницу person.jsp.
     * Используется {@link sbp.jdbc.dao.WorkWithTable}.
     * @param req
     * @param resp
     * @throws ServletException
     * @throws IOException
     */
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException
    {
        int id = Integer.parseInt(req.getParameter("id"));

        Person person = null;
        try
        {
            person = personRepo.findPersonById(id);
        } catch (Exception e)
        {
            e.printStackTrace();
        }

        req.setAttribute("person", person);
        getServletContext().getRequestDispatcher("/person.jsp").forward(req, resp);
    }

    /**
     * Обработка POST запроса.
     * Редактирование пользователя в бвзе данных по ID
     * с использованием {@link sbp.jdbc.dao.WorkWithTable}.
     * @param req
     * @param resp
     */
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp)
    {
        int id = Integer.parseInt(req.getParameter("id"));
        String personName = req.getParameter("personName");
        String city = req.getParameter("city");
        int age = Integer.parseInt(req.getParameter("age"));

        Person person = null;
        try
        {
            person = personRepo.findPersonById(id);
        } catch (Exception e)
        {
            e.printStackTrace();
        }
        person.setPersonName(personName);
        person.setCity(city);
        person.setAge(age);
        personRepo.updatePerson(person);

        try
        {
            resp.sendRedirect("http://localhost:8080/Lecture6_war");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}

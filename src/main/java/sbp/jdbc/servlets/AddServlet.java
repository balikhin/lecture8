package sbp.jdbc.servlets;

import sbp.jdbc.dao.WorkWithTable;
import sbp.jdbc.model.Person;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Сервлет добавления нового пользователя
 */
@WebServlet("/person/add")
public class AddServlet extends HttpServlet
{

    WorkWithTable personRepo = new WorkWithTable();

    /**
     * Обработка GET запроса.
     * Перенаправляет на страницу редактирования пользователя.
     * @param req
     * @param resp
     * @throws ServletException
     * @throws IOException
     */
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException
    {
        req.getRequestDispatcher("person.jsp").forward(req, resp);
    }

    /**
     * Обработка POST запроса.
     * Создание нового пользователя в базе данных
     * с использованием {@link sbp.jdbc.dao.WorkWithTable}.
     * После добавление возвращает на стартовую страницу.
     * @param req
     * @param resp
     */
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp)
    {
        String personName = req.getParameter("personName");
        String city = req.getParameter("city");
        int age = Integer.parseInt(req.getParameter("age"));

        Person person = new Person(personName, city, age);
        personRepo.createPerson(person);

        try
        {
            resp.sendRedirect("http://localhost:8080/Lecture6_war");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}

package sbp.jdbc.model;

import java.util.Objects;

public class Person implements Comparable<Person>
{
    private int id;
    private String personName;
    private String city;
    private int age;

    public Person(String personName, String city, int age)
    {
        if (personName == null || city == null)
        {
            throw new NullPointerException();
        }
        this.id = 0;
        this.personName = personName;
        this.city = city;
        this.age = age;
    }

    public Person(int id, String personName, String city, int age)
    {
        if (personName == null || city == null)
        {
            throw new NullPointerException();
        }
        this.id = id;
        this.personName = personName;
        this.city = city;
        this.age = age;
    }

    public int getId() {
        return id;
    }

    public String getPersonName() {
        return personName;
    }

    public void setPersonName(String personName) {
        this.personName = personName;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    /**
     *  Сортировка по городу и по имени при одинаковых городах.
     * @param o - объект с которым происходит сравнение
     * @return - 0 если объекты равны.
     * Если город находится раньше (в алфавитном порядке)
     * или (при равных городах) Имя ближе к началу алфавита, то возращается -1.
     * Иначе 1.
     */
    @Override
    public int compareTo(Person o)
    {
        if (getCity().equals(o.getCity()))
        {
            return getPersonName().compareTo(o.getPersonName());
        }
        return getCity().compareTo(o.getCity());
    }

    /**
     * Метод сравнения обьектов
     * @param o принимаемый параметр
     * @return true, если ссылки на обьекты равны или обьекты равны
     * @return false, если принимаемы обьект null или классы обьектов различны
     */
    @Override
    public boolean equals(Object o)
    {
        if (this == o) return true;
        if (o == null || !(o instanceof Person)) return false;
        Person person = (Person) o;
        return age == person.age && Objects.equals(personName, person.personName) && Objects.equals(city, person.city);
    }

    /**
     * Генерирует hashCode
     * @return hashCode
     */
    @Override
    public int hashCode()
    {
        return Objects.hash(personName, city, age);
    }

    @Override
    public String toString()
    {
        return "Person{" +
                "name='" + personName + '\'' +
                ", city='" + city + '\'' +
                ", age=" + age +
                '}';
    }
}